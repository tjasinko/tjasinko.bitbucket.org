
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var imena = ["Jaka", "Matej", "Lukas"];
var priimki = ["Šemrov", "Miočič", "Jeglič"];
var teze = [100, 30, 80];
var visine = [160, 140, 190];

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  authorization = getAuthorization();

	$.ajaxSetup({
	    headers: {
	        "Authorization": authorization
	    }
	});
	resp = $.ajax({
       async: false,
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
	
	        // build party data
	        var partyData = {
	            firstNames: imena[stPacienta],
	            lastNames: priimki[stPacienta],
	            //dateOfBirth: "1982-7-18T19:30",
	            partyAdditionalInfo: [
	                {
	                    key: "ehrId",
	                    value: ehrId
	                }
	            ]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    $("#result").html("Created: " + party.meta.href);
	                }
	            }
	        });
	    }
	});
	
	ehrId = resp.responseJSON.ehrId;
	
	$.ajaxSetup({
       headers: {
           "Authorization": authorization
       }
   });
   var compositionData = {
     "ctx/language": "en",
     "ctx/territory": "SI",
     "vital_signs/height_length/any_event/body_height_length": visine[stPacienta],
     "vital_signs/body_weight/any_event/body_weight": teze[stPacienta]
   };
   var queryParams = {
     "ehrId": ehrId,
     templateId: 'Vital Signs',
     format: 'FLAT'
   };
   $.ajax({
     async: false,
     url: baseUrl + "/composition?" + $.param(queryParams),
     type: 'POST',
     contentType: 'application/json',
     data: JSON.stringify(compositionData),
   });

   return ehrId;
}

function pridobiPodatke() {
	authorization = getAuthorization();
   ehrId = document.getElementById("ehr_input").value

	$.ajax({
	    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    type: 'GET',
	    headers: {
	        "Authorization": authorization
	    },
	    success: function (data) {
	        var party = data.party;
	        document.getElementById("ime").value = party.firstNames;
	        document.getElementById("priimek").value = party.lastNames;
	    }
	});
	$.ajax({
	    url: baseUrl + "/view/" + ehrId + "/weight",
	    type: 'GET',
	    headers: {
	        "Authorization": authorization
	    },
	    success: function (res) {
	      document.getElementById("teza").value = res[0].weight
	    }
	});
	$.ajax({
	    url: baseUrl + "/view/" + ehrId + "/height",
	    type: 'GET',
	    headers: {
	        "Authorization": authorization
	    },
	    success: function (res) {
	      document.getElementById("visina").value = res[0].height
	    }
	});
}

function izracunITM() {
  teza = parseInt(document.getElementById("teza").value);
  visina = parseInt(document.getElementById("visina").value);

  if(teza > 0 && visina > 0){
    itm = teza/(visina/100*visina/100);
    document.getElementById("ITMStevilo").innerHTML = itm.toFixed(2);
    console.log(itm);
    if(itm < 18.5){
      document.getElementById("ITMText").innerHTML = "podhranjenost"
    }
    else if(itm > 18.5 && itm < 25){
      document.getElementById("ITMText").innerHTML = "primerna teža"
    }
    else if(itm > 25){
      document.getElementById("ITMText").innerHTML = "prekomerna teža"
    }
  }
  else{
    alert("Napaka pri izračunu ITM");
  }
}

function generirajPaciente() {
  if (document.getElementById("izborPacient").children.length > 1) return;
  for (i=0; i<3; i++) {
  	 patientId = generirajPodatke(i);
  	 element = document.createElement("option");
  	 element.setAttribute("value", patientId);
  	 element.innerHTML = imena[i] + " " + priimki[i];
  	 document.getElementById("izborPacient").appendChild(element);
  }
  alert("Generirani so bili 3je pacienti. Izberete jih lahko v meniju poleg polja za vnos Ehr ID-ja.");
}

function prikaziZivila(zivilo) {
  zivila = JSON.parse(hrana_json);
  vrednosti = zivila[zivilo];
  document.getElementById("zivilo_porcija").innerHTML = vrednosti["porcija"];
  document.getElementById("zivilo_kalorije").innerHTML = vrednosti["kalorije"];
  document.getElementById("zivilo_oh").innerHTML = vrednosti["oh"];
  document.getElementById("zivilo_beljakovine").innerHTML = vrednosti["beljakovine"];
  document.getElementById("zivilo_mascobe").innerHTML = vrednosti["mascobe"];
  document.getElementById("zivilo_vlaknine").innerHTML = vrednosti["vlaknine"];
  document.getElementById("zivilo_sladkorji").innerHTML = vrednosti["sladkorji"];
}

$(document).ready(function(){
	document.getElementById("get_data").onclick = pridobiPodatke;
	document.getElementById("generirajPodatke").onclick = generirajPaciente;
	document.getElementById("izborPacient").onchange = function () {
	  e = document.getElementById("izborPacient")
	  document.getElementById("ehr_input").value = e.options[e.selectedIndex].value;
	};
	document.getElementById("izborHrana").onchange = function () {
	  e = document.getElementById("izborHrana")
	  prikaziZivila(e.options[e.selectedIndex].value);
	};
})

var poligoni = [];

var mapa;

const POS_LAT = 46.05105;
const POS_LNG = 14.50744;

window.addEventListener('load', function () {

  var mapOptions = {
    center: [POS_LAT, POS_LNG],
    zoom: 12
  };

  mapa = new L.map('mapa_bolnisnice', mapOptions);
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  mapa.addLayer(layer);

  function obKlikuNaMapo(e) {
    posodobiOznakeNaZemljevidu(e.latlng);
  }

  mapa.on('click', obKlikuNaMapo);
  
  posodobiOznakeNaZemljevidu({'lat':POS_LAT,'lng':POS_LNG});
  
});

function dodajBolnisnice(latlng) {
  pridobiPodatkeZ(function (jsonRezultat) {
    izrisRezultatov(jsonRezultat, latlng);
  });
}

function pridobiPodatkeZ(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        json.stRezultatov = json.features.length;

        callback(json);
    }
  };
  xobj.send(null);
}

function dodajPoligon(koordinate, podatki, pozlng, pozlat) {
  barva = radijBarva(koordinate[0][0][1], koordinate[0][0][0], pozlng, pozlat) ? 'green' : 'blue'
  var polygon = L.polygon(koordinate, {color: barva})
  polygon.bindPopup("<div><center>" +podatki.name+ "<br><br>" +podatki['addr:street']+ " " +podatki['addr:housenumber']+ ", " +podatki['addr:postcode']+ " " +podatki['addr:city']+ "</center></div>");
  polygon.addTo(mapa);
  poligoni.push(polygon);
}

function izrisRezultatov(jsonRezultat, latlng) {
  var znacilnosti = jsonRezultat.features;

  for (var i = 0; i < znacilnosti.length; i++) {

    var jeObmocje = znacilnosti[i].geometry.type == "Polygon";

    if (jeObmocje) {
    	koordinate = znacilnosti[i].geometry.coordinates.map(function (vrednost) {
    	  new_vrednost = vrednost.map(function (koordinati) {
    	  	 return [koordinati[1], koordinati[0]];
    	  });
    	  return new_vrednost;
      });
      dodajPoligon(koordinate, znacilnosti[i].properties, latlng.lng, latlng.lat);
    }
  }
}

function posodobiOznakeNaZemljevidu(latlng) {
  for(var i = 0; i < poligoni.length; i++) {
    mapa.removeLayer(poligoni[i]);
  }
  dodajBolnisnice(latlng);
}


function radijBarva(lng, lat, pozlng, pozlat) {
  var radij = 2;
  if (radij == 0)
    return true;
  else if (distance(lat, lng, pozlat, pozlng, "K") >= radij) 
    return false;
  else
    return true;
}