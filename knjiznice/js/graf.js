window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	title:{
		text: "Delež prebivalcev, ki so glede na ITM debeli"
	},
	subtitles: [{
		text: "Po spolu, Slovenija"
	}], 
	axisX: {
		title: "Leto"
	},
	axisY: {
		title: "Odstotki",
		titleFontColor: "#4F81BC",
		lineColor: "#4F81BC",
		labelFontColor: "#4F81BC",
		tickColor: "#4F81BC"
	},
	toolTip: {
		shared: true
	},
	legend: {
		cursor: "pointer"
	},
	data: [{
		type: "column",
		name: "Moški",
		showInLegend: true,      
		yValueFormatString: "0.0'%'",
		dataPoints: [
			{ label: "2001",  y: 16.2 },
			{ label: "2004", y: 16.0 },
			{ label: "2008", y: 18.4 },
			{ label: "2012",  y: 20.7 },
			{ label: "2016",  y: 20.0 }
		]
	},
	{
		type: "column",
		name: "Ženske",
		showInLegend: true,
		yValueFormatString: "0.0'%'",
		dataPoints: [
			{ label: "2001",  y: 13.8 },
			{ label: "2004", y: 13.2 },
			{ label: "2008", y: 13.9 },
			{ label: "2012",  y: 14.0 },
			{ label: "2016",  y: 14.6 }
		]
	},
	{
		type: "column",
		name: "Skupaj",
		showInLegend: true,
		yValueFormatString: "0.0'%'",
		dataPoints: [
			{ label: "2001",  y: 15.0 },
			{ label: "2004", y: 14.6 },
			{ label: "2008", y: 16.2 },
			{ label: "2012",  y: 17.4 },
			{ label: "2016",  y: 17.4 }
		]
	}]
});
chart.render();
}