hrana_json = `{
	"blank": {
		"porcija": "-",
		"kalorije": "-",
		"oh": "-",
		"beljakovine": "-",
		"mascobe": "-",
		"vlaknine": "-",
		"sladkorji": "-"
	},
	"jabolko": {
		"porcija": "100g",
		"kalorije": "52kcal",
		"oh": "14g",
		"beljakovine": "0.3g",
		"mascobe": "0.2g",
		"vlaknine": "2.4g",
		"sladkorji": "10g"
	},
	"banana": {
		"porcija": "100g",
		"kalorije": "89kcal",
		"oh": "23g",
		"beljakovine": "1.1g",
		"mascobe": "0.3g",
		"vlaknine": "2.6g",
		"sladkorji": "12g"
	},
	"pomaranca": {
		"porcija": "100g",
		"kalorije": "49kcal",
		"oh": "13g",
		"beljakovine": "0.9g",
		"mascobe": "0.2g",
		"vlaknine": "2.2g",
		"sladkorji": "8.5g"
	},
	"korenje": {
		"porcija": "100g",
		"kalorije": "41kcal",
		"oh": "9.6g",
		"beljakovine": "0.9g",
		"mascobe": "0.2g",
		"vlaknine": "2.8g",
		"sladkorji": "4.7g"
	},
	"paradiznik": {
		"porcija": "100g",
		"kalorije": "16kcal",
		"oh": "3.2g",
		"beljakovine": "1.2g",
		"mascobe": "0.2g",
		"vlaknine": "0.9g",
		"sladkorji": "2.6g"
	},
	"krompir": {
		"porcija": "100g",
		"kalorije": "58kcal",
		"oh": "12g",
		"beljakovine": "2.6g",
		"mascobe": "0.1g",
		"vlaknine": "2.5g",
		"sladkorji": "/"
	},
	"spageti": {
		"porcija": "100g",
		"kalorije": "372kcal",
		"oh": "75g",
		"beljakovine":"13g",
		"mascobe": "1.6g",
		"vlaknine": "11g",
		"sladkorji": "2.7g"
	},
	"bel_kruh": {
		"porcija": "100g",
		"kalorije": "238kcal",
		"oh": "44g",
		"beljakovine": "11g",
		"mascobe": "2.2g",
		"vlaknine": "9.2g",
		"sladkorji": "5g"
	},
	"piscancje_prsi_surove": {
		"porcija": "100g",
		"kalorije": "124kcal",
		"oh": "/",
		"beljakovine": "26.1g",
		"mascobe": "1.4g",
		"vlaknine": "/",
		"sladkorji": "/"
	},
	"jajca": {
		"porcija": "100g",
		"kalorije": "143kcal",
		"oh": "0.7g",
		"beljakovine": "13g",
		"mascobe": "9.5g",
		"vlaknine": "/",
		"sladkorji": "0.4g"
	},
	"sir_edamec": {
		"porcija": "100g",
		"kalorije": "357kcal",
		"oh": "1.4g",
		"beljakovine": "25g",
		"mascobe": "28g",
		"vlaknine": "/",
		"sladkorji": "1.4g"
	},
	"grski_jogurt": {
		"porcija": "100g",
		"kalorije": "73kcal",
		"oh": "3.9g",
		"beljakovine": "10g",
		"mascobe": "1.9g",
		"vlaknine": "/",
		"sladkorji": "3.6g"
	},
	"majoneza_thomy": {
		"porcija": "100g",
		"kalorije": "713kcal",
		"oh": "1.9g",
		"beljakovine": "1.1g",
		"mascobe": "77.8g",
		"vlaknine": "0.1g",
		"sladkorji": "1.8g"
	},
	"milka_cokolada": {
		"porcija": "42g",
		"kalorije": "230kcal",
		"oh": "25g",
		"beljakovine": "3g",
		"mascobe": "12g",
		"vlaknine": "0.5g",
		"sladkorji": "24g"
	},
	"soncnicno_olje": {
		"porcija": "42g",
		"kalorije": "230kcal",
		"oh": "25g",
		"beljakovine": "3g",
		"mascobe": "12g",
		"vlaknine": "0.5g",
		"sladkorji": "24g"
	},
	"maslo": {
		"porcija": "100g",
		"kalorije": "717kcal",
		"oh": "0.1g",
		"beljakovine": "0.9g",
		"mascobe": "81g",
		"vlaknine": "/",
		"sladkorji": "0.1g"
	},
	"mleko": {
		"porcija": "100g",
		"kalorije": "60kcal",
		"oh": "4.5g",
		"beljakovine": "3.2g",
		"mascobe": "3.3g",
		"vlaknine": "/",
		"sladkorji": "5.3g"
	}
}`